# PFASDR.Samples.Linear_20_100

## Create a virtual environment

    python3.8 -m venv venv

## Install virtual environment requirements

     venv/bin/python -m pip install --quiet --upgrade -r requirements.d/venv.txt

## Create runtime virtual environment

     venv/bin/python -m tox -e py38

## Package the data files

    venv/bin/python setup.py bdist
