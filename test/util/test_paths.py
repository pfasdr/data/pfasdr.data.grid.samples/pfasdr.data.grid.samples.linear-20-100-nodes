from pathlib import Path

from pfasdr_grid_linear_20_100.grid_linear_20_100.util import paths


def test_root_path_type(root_path):
    assert isinstance(root_path, Path)


def test_root_path_as_path_object(root_path):
    assert root_path == Path(__file__).parent.parent.parent


def test_root_path_as_string_object(root_path):
    assert str(root_path) == str(Path(__file__).parent.parent.parent)


def test_paths_are_strings_or_paths():
    global_paths = [item for item in dir(paths) if not item.startswith("__")]
    for member in global_paths:
        assert type(member) in (str, Path)
