import pytest

from pfasdr_grid_linear_20_100.grid_linear_20_100.util.paths import ROOT_PATH


@pytest.fixture(name='root_path')
def root_path_fixture():
    return ROOT_PATH
