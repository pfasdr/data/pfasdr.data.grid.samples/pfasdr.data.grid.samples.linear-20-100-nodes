from os.path import abspath
from pathlib import Path

ROOT_PATH: Path = Path(abspath(__file__)).parent.parent.parent.parent

DATA_PATH: Path = ROOT_PATH / 'data'

LINEAR_20_PATH: Path = DATA_PATH / 'length_20'
LINEAR_50_PATH: Path = DATA_PATH / 'length_50'
LINEAR_100_PATH: Path = DATA_PATH / 'length_100'
